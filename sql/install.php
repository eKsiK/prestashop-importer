<?php
/**
* 2007-2017 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2017 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

$sql = array();

$sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'ti_products` (
	`id` int(11) NOT NULL,
	`isbn` varchar(13) NOT NULL,
	`id_azymut` varchar(100) NOT NULL,
	`id_ateneum` varchar(100),
	`azymut_price` decimal(9,2),
	`azymut_wholesale` decimal(9,2),
	`azymut_vat` int(2),
	`ss_price` decimal(9,2),
	`ss_wholesale` decimal(9,2),
	`ss_vat` int(2),
	`ateneum_price` decimal(9,2),
	`ateneum_wholeprice` decimal(9,2),
	`ateneum_vat` int(2),
	`tytuloryg` varchar(100) NULL,
	`jezykoryg` varchar(100) NULL,
	`tlumacze` varchar(100) NULL,
	`redakcja` varchar(100) NULL,
	`oprawa` varchar(100) NULL,
	`format` varchar(100) NULL,
	`objetosc` varchar(100) NULL,
	`wydanie` varchar(100) NULL,
	`rokwyd` varchar(100) NULL,
	`kodwydawcy` varchar(100) NULL,
	`ciezar` varchar(100) NULL,
	`tytultomu` varchar(100) NULL,
	`nrkolejnyt` varchar(100) NULL,
	`liczbatomo` varchar(100) NULL,
	`pkwiu` varchar(100) NULL,
	`issn` varchar(100) NULL
	
) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;';

$sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'ti_categories` (
    `id` int(11) NOT NULL,
	`id_azymut` varchar(10) NOT NULL,
	`id_azymut_cat` varchar(10),
	`id_parent` int(11) NOT NULL
	
) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;';

$sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'ti_options` (
	`id` int(11) NOT NULL,
	`option_name` varchar(100) NOT NULL,
	`option_value` varchar(100) NOT NULL,
	`max_value` varchar(100) NOT NULL,
	`update_data` TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL
	
	
) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8;';

$sql[] = 'ALTER TABLE `'._DB_PREFIX_.'ti_options` ADD PRIMARY KEY (`id`);';

$sql[] = 'ALTER TABLE `'._DB_PREFIX_.'ti_options` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;COMMIT;';

$sql[] = 'INSERT INTO `'._DB_PREFIX_.'ti_options` (`id`, `option_name`, `option_value`) VALUES (NULL, "last_image_id", "0");';

$sql[] = 'INSERT INTO `'._DB_PREFIX_.'ti_options` (`id`, `option_name`, `option_value`) VALUES (NULL, "last_category_id", "0");';

$sql[] = 'INSERT INTO `'._DB_PREFIX_.'ti_options` (`id`, `option_name`, `option_value`) VALUES (NULL, "last_avail_id", "0");';

$sql[] = 'INSERT INTO `'._DB_PREFIX_.'ti_options` (`id`, `option_name`, `option_value`) VALUES (NULL, "last_price_id", "0");';

$sql[] = 'INSERT INTO `'._DB_PREFIX_.'ti_options` (`id`, `option_name`, `option_value`) VALUES (NULL, "last_ss_id", "0");';

$sql[] = 'INSERT INTO `'._DB_PREFIX_.'ti_options` (`id`, `option_name`, `option_value`) VALUES (NULL, "last_ateneum_db_id", "0");';

$sql[] = 'INSERT INTO `'._DB_PREFIX_.'ti_options` (`id`, `option_name`, `option_value`) VALUES (NULL, "last_ateneum_price_id", "0");';

$sql[] = 'INSERT INTO `'._DB_PREFIX_.'ti_options` (`id`, `option_name`, `option_value`) VALUES (NULL, "last_best_price_id", "0");';

foreach ($sql as $query) 
{
    if (Db::getInstance()->execute($query) == false) 
	{
        return false;
    }
}
