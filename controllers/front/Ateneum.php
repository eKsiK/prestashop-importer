<?php

include_once 'Importer.php';

/**
 * Description of Ateneum
 *
 * @author Paweł Kujaczyński for Tomsky Sp. z o.o.
 */
class Ateneum extends Importer
{
	/**
	 * Ateneum user.
	 * @var string
	 */
	private $ateneum_user = 'stanczyk';
	
	/**
	 * Ateneum password.
	 * @var string
	 */
	private $ateneum_passwd = 'A3aRgmu3Fx';
	
	/**
	 * Books database file url.
	 * @var string
	 */
	private $books_db_url = 'http://ateneum.net.pl/bazaksiazek/baza_ksiazek.csv';
	
	/**
	 * Prices file url.
	 * @var string
	 */
	private $price_url = 'http://ateneum.net.pl/stanczyk/ceny.csv';
	
	private $limit_data = 1000;
	
	/**
	 * Import data from Ateneum.
	 * @param string $mode
	 */
	public function __construct($mode)
	{
		if ($mode)
		{
			switch ($mode)
			{
				case 'getdb':
					$this->getDbData($mode);
					break;
				case 'getPrice':
					$this->getPriceData($mode);
					break;
				default:
					$this->saveLog('[Ateneum] Method unknown.');
					break;
			}
		}
		else
		{
			$this->saveLog('[Ateneum] Method not exist.');
		}
	}
	
	/**
	 * Get Ateneum db data.
	 */
	public function getDbData()
	{
		$this->saveLog('[Ateneum] Start: getdb.');
		
		$file = __DIR__.'/ateneum_db.txt';
		
		$data = $this->getDataFromAteneum($this->books_db_url, $file);
		
		$lines = explode(PHP_EOL, $data);
		
		if ($lines)
		{
			$last_index = (int)ImporterModel::getOptionValueByName('last_ateneum_db_id');
			$last_product_index = $last_index;

			$n = count($lines);
			
			ImporterModel::updateMaxValueByName('last_ateneum_db_id', $n);
			
			for($i = $last_index; $i <= $last_index+$this->limit_data; $i++)
			{
				$book = str_getcsv($lines[$i]);
		
				if ($book[2] && (($prod_id = ImporterModel::getProductIdByIsbn($book[2])) != false))
				{
					$product_data = array(
						'id_ateneum' => $book[0],
						'ateneum_price' => $book[9],
					);
					
					$where = 'id='.$prod_id;
					
					Db::getInstance()->update(ImporterModel::TAB_PRODS, $product_data, $where);
				}
				$last_product_index = $i;
				
				ImporterModel::updateOptionValueByName('last_ateneum_db_id', $last_product_index);

				if ($i >= $n)
				{
					ImporterModel::updateOptionValueByName('last_ateneum_db_id', '0');
					unlink($file);
					break;
				}
			}
			$this->saveLog('[Ateneum] Last index: '.($last_product_index));
		}
		
		$this->saveLog('[Ateneum] End: getdb.');
	}
	
	/**
	 * Get Ateneum Price data.
	 */
	public function getPriceData()
	{
		$this->saveLog('[Ateneum] Start: getPrice.');
		
		$file = __DIR__.'/ateneum_price.txt';
		
		$data = $this->getDataFromAteneum($this->price_url, $file);
		
		$lines = explode(PHP_EOL, $data);
		
		if ($lines)
		{
			$last_index = (int)ImporterModel::getOptionValueByName('last_ateneum_price_id');
			$last_product_index = $last_index;

			$n = count($lines);
			
			ImporterModel::updateMaxValueByName('last_ateneum_price_id', $n);
			
			for($i = $last_index; $i <= $last_index+$this->limit_data; $i++)
			{
				$book = str_getcsv($lines[$i]);
				
				if ($book[0])
				{
					$price = round(100*$book[1]/(100+$book[3]),2);
						
					$product_data = array(
						'ateneum_price' => $price,
						'ateneum_wholesale' => $book[2],
						'ateneum_vat' => $book[3]
					);
					
					$where = 'id_ateneum="'.$book[0].'"';
					
					Db::getInstance()->update(ImporterModel::TAB_PRODS, $product_data, $where);
				}
				
				$last_product_index = $i;
				
				ImporterModel::updateOptionValueByName('last_ateneum_price_id', $last_product_index);

				if ($i >= $n)
				{
					ImporterModel::updateOptionValueByName('last_ateneum_price_id', '0');
					unlink($file);
					break;
				}
			}
			$this->saveLog('[Ateneum] Last index: '.($last_product_index));
		}
		
		$this->saveLog('[Ateneum] End: getPrice.');
	}
	
	/**
	 * Get Data from Ateneum by url.
	 * @param string $url
	 * @return array
	 */
	public function getDataFromAteneum($url, $file)
	{	
		if (!file_exists($file))
		{
			$data = $this->getDataFromUrlFile($url, $this->ateneum_user, $this->ateneum_passwd);
			
			$handle = fopen($file, 'a+');
			fwrite($handle, $data);
		}
		else
		{
			$handle = fopen($file, 'a+');
			$data = fread($handle, filesize($file));
		}
		fclose($handle);
		
		return $data;
	}
}
