<?php

// Include Importer class.
include_once _PS_MODULE_DIR_.'/tomskyimporter/models/ImporterModel.php';
include_once 'Importer.php';
include_once 'Azymut.php';
include_once 'Ateneum.php';
include_once 'Supersiodemka.php';

/**
 * Import data from Azymut, Ateneum, Super Siódemka.
 * 
 * @autor Paweł kujaczyński for Tomsky Sp. z o.o.
 */
class TomskyimporterCronModuleFrontController extends ModuleFrontController
{
	/**
	 * Init.
	 */
	public function init()
	{
		$ti_mode = Tools::getValue('mode');
		$ti_method = Tools::getValue('method');
		$ti_debug = Tools::getValue('debug');
		$ti_attr = Tools::getValue('attr');
		
		if ($ti_debug)
		{
			error_reporting(E_ERROR | E_WARNING | E_PARSE);
		}
		
		if (file_exists(__DIR__.'/cron_error'))
		{
			exit();
		}
		
		$importer = new Importer($ti_debug);
		
		$importer->saveLog('[Cron] Start at: '.date('d-m-Y H:i:s'));
		
		if ($ti_mode)
		{
			switch ($ti_mode)
			{
				case 1:
					$azymut = new Azymut($ti_method, $ti_debug, $ti_attr);
					break;
				case 2:
					$ateneum = new Ateneum($ti_method);
					break;
				case 3:
					$supersiodemka = new Supersiodemka($ti_method);
					break;
				case 4:
					$importer->saveLog('[Price] Start: bestPrice.');
					$importer->setAllBestProductPrice();
					$importer->saveLog('[Price] End: bestPrice.');
					break;
				default:
					$importer->saveLog('[Cron] Unknown mode.');
					break;
			}
		}
		else
		{
			$importer->saveLog('[Cron] Mode not exist.');
		}
	}
}