<?php

include_once 'Importer.php';

/**
 * Description of azymut
 *
 * @author Paweł Kujaczyński for Tomsky Sp. z o.o.
 */
class Supersiodemka extends Importer
{
	/**
	 * Siodemka FTP server.
	 * @var string 
	 */
	private $siodemka_ftp_server = '213.218.125.80';
	
	/**
	 * Siodemka FTP login.
	 * @var string
	 */
	private $siodemka_ftp_login = 'stanczyk';
	
	/**
	 * Siodemka FTP password.
	 * @var string
	 */
	private $siodemka_ftp_passwd = 'StaFTP1';
	
	/**
	 * Siodemka FTP port.
	 * @var string
	 */
	private $siodemka_ftp_port = '22';
	
	/**
	 * Siodemka file dir.
	 * @var string
	 */
	private $siodemka_file_dir = '/PlikiS7/4526.txt';
	
	/**
	 * Limit data for one cron.
	 * @var int
	 */
	private $limit_data = 1000;
	
	/**
	 * Import data from Super Siodemka.
	 * @param string $method
	 */
	public function __construct($mode)
	{
		$this->getPrice();
	}
	
	/**
	 * Get Price
	 */
	public function getPrice()
	{
		$this->saveLog('[S7] Start: getPrice.');
		
		$file = __DIR__.'/super_siodemka.txt';
		
		$data = $this->getDataFromSiodemka($file);
		
		if ($data)
		{
			$last_index = (int)ImporterModel::getOptionValueByName('last_ss_id');
			$last_product_index = $last_index;
			
			$n = count($data);
			
			ImporterModel::updateMaxValueByName('last_ss_id', $n);
			
			for($i = $last_index; $i <= $last_index+$this->limit_data; $i++)
			{
				$book_array = $row = preg_split("/[\t]/", $data[$i]);
				
				if ($book_array[6] && (($prod_id = ImporterModel::getProductIdByIsbn($book_array[6])) != false))
				{
					$price_data = array(
						'ss_price' => $book_array[2],
						'ss_wholesale' => 0.00,
						'ss_vat' => ($book_array[3]*100),
					);
					
					$where = 'id='.$prod_id;
					
					Db::getInstance()->update(ImporterModel::TAB_PRODS, $price_data, $where);
					
					$last_product_index = $i;
				}

				ImporterModel::updateOptionValueByName('last_ss_id', $last_product_index);

				if ($i >= $n)
				{
					ImporterModel::updateOptionValueByName('last_ss_id', '0');
					unlink($file);
					break;
				}
			}
		
			$this->saveLog('[S7] Last index: '.$last_product_index);
		}
		
		$this->saveLog('[S7] End: getPricde.');
	}
	
	/**
	 * Get data from Super Siódemka and save to file.
	 * @return array
	 */
	public function getDataFromSiodemka($file)
	{
		if (!file_exists($file))
		{
			$this->getDataFromFtp(
				$this->siodemka_ftp_server, 
				$this->siodemka_ftp_login, 
				$this->siodemka_ftp_passwd,
				$this->siodemka_ftp_port,
				$this->siodemka_file_dir,
				$file
			);
		}

		$handle = fopen($file, 'a+');
		$data_string = fread($handle, filesize($file));
		fclose($handle);
		
		$data = explode(PHP_EOL, $data_string);
		
		return $data;
	}
}
