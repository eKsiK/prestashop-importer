<?php

include_once 'Importer.php';

/**
 * Description of azymut
 *
 * @author Paweł Kujaczyński for Tomsky Sp. z o.o.
 */
class Azymut extends Importer
{
	/**
	 * Azymut client ID.
	 * @var string
	 */
	private $azymut_id = '11125';
	
	/**
	 * Azymut client password.
	 * @var string
	 */
	private $azymut_passwd = 'esqT4suS';
	
	/**
	 * Azymut page url.
	 * @var string
	 */
	private $azymut_url = 'http://services.azymut.pl';
	
	/**
	 * Azymut Services url.
	 * @var string
	 */
	private $azymut_serv_url = '/oferta/servlet/';
	
	/**
	 * Azymut `getdb` transaction id.
	 * @var string
	 */
	private $transaction_Id = '';
	
	/**
	 * Image limit for one loop.
	 * @var int
	 */
	private $img_limit_pl = 100;
	
	/**
	 * Xml limit data for one loop.
	 * @var int
	 */
	private $xml_limit_data = 1000;
	
	/**
	 * Xml limit data for one loop for cat2end.
	 * @var int
	 */
	private $xml_c2e_limit_data = 500;
	
	/**
	 * Import data from Azymut
	 * @param string $mode Method type: getImg, getAvail, getPrice, getdb, getCats, getDzialy, getInd2Cat, getItem.
	 * @param bool $debug
	 */
	public function __construct($mode, $debug = true, $attr = false)
	{
		parent::__construct($debug);
		
		if ($mode)
		{
			switch ($mode)
			{
				case 'getdb':
					$this->getDbData($mode);
					break;
				case 'getPrice':
					$this->getPriceData($mode);
					break;
				case 'getImg':
					$this->getImgData($mode, $attr);
					break;
				case 'getCats':
					$this->getCatsData($mode);
					break;
				case 'getDzialy':
					$this->getDzialyData($mode);
					break;
				case 'getAvail':
					$this->getAvailData($mode);
					break;
				case 'getItem':
					//$this->getItemData($mode, $index);
					break;
				case 'getInd2Cat':
					$this->getInd2CatData($mode);
					break;
				default:
					$this->saveLog('[Azymut] Method unknown.');
					break;
			}
		}
		else
		{
			$this->saveLog('[Azymut] Method not exist.');
		}
	}
	
	/**
	 * Conifrm Transation.
	 * @param string $transactionId
	 */
	public function confirmTransaction($transactionId)
	{
		if ($transactionId != '')
		{
			$this->getDataFromAzymut('confirm', '&transactionId='.$transactionId);
		}
	}
	
	/**
	 * Import Azymut main database.
	 * @param string $mode
	 */
	public function getDbData($mode)
	{
		$this->saveLog('[Azymut] Start: getdb.');
		
		$xml_data = $this->getDataFromAzymut($mode);
		
		$error = false;
		
		if ($xml_data)
		{	
			$xml = simplexml_load_string($xml_data, "SimpleXMLElement", LIBXML_NOCDATA);
			$data = $this->xmlToArray($xml);
			
			$this->transaction_Id = $data['stuff']['@transactionId'];
			
			foreach ($data['stuff']['books']['book'] as $book)
			{
				if ($error)
				{
					break;
				}
				
				try 
				{
					$id_azymut = $book['@indeks'];
				
					// Get only Dzial `KC` and `ZBW`.
					if ($book['@dzial'] != 'KC' && $book['@dzial'] != 'ZBW' && !is_numeric($id_azymut))
					{
						continue;
					}

					if (($id_prod = ImporterModel::getProductIdByAzymutIndex($id_azymut)) != false)
					{
						$product = new Product((int)$id_prod);
					}
					else
					{
						$product = new Product();
					}

					if (!empty($book['wydawca']) && (($id_manufacturer = Manufacturer::getIdByName($book['wydawca'])) == false))
					{
						$manufacturer = new Manufacturer();
						$manufacturer->name = substr($book['wydawca'], 0, 60);
						$manufacturer->active = true;
						$manufacturer->save();

						$id_manufacturer = $manufacturer->id_manufacturer;
					}

					if (!empty($book['wydawca']))
					{
						$product->id_manufacturer = $id_manufacturer;
					}

					$authors = (is_array($book['autorzy']) ? $book['autorzy'] : array($book['autorzy']));

					$languages = Language::getLanguages(true);

					$short_description = $this->makeShortDescription($book);
					
					foreach ($languages as $lang)
					{
						$name = $this->cleanString(mb_strcut($book['tytul'], 0, 110));
						$description = $this->cleanString($book['opis']);
						
						$product->name[$lang['id_lang']] = $name;
						$product->link_rewrite[$lang['id_lang']] = Tools::link_rewrite(mb_strcut($name, 0, 110));
						$product->description[$lang['id_lang']] = $description;
						$product->description_short[$lang['id_lang']] = mb_strcut($short_description, 0, 790);
					}

					if (!empty($book['kod_paskowy']) && strlen($book['kod_paskowy']) == 13)
					{
						$product->ean13 = $book['kod_paskowy'];
					}

					$product->price = $book['cenaDet'];
					$product->wholesale_price = $book['cenaHurt'];
					$product->save();

					$data = array(
						'id' => $product->id,
						'isbn' => $book['isbn'],
						'id_azymut' => $id_azymut,
						'azymut_price' => $book['cenaDet'],
						'azymut_wholesale' => $book['cenaHurt'],
						'tytuloryg' => $book['tytuloryg'],
						'jezykoryg' => $book['jezykoryg'],
						'tlumacze' => $book['tlumacze'],
						'redakcja' => $book['redakcja'],
						'oprawa' => $book['oprawa'],
						'format' => $book['format'],
						'objetosc' => $book['objetosc'],
						'wydanie' => $book['wydanie'],
						'rokwyd' => $book['rokwyd'],
						'kodwydawcy' => $book['kodwydawcy'],
						'ciezar' => $book['ciezar'],
						'tytultomu' => $book['tytultomu'],
						'nrkolejnyt' => $book['nrkolejnyt'],
						'liczbatomo' => $book['liczbatomo'],
						'pkwiu' => $book['pkwiu'],
						'issn' => $book['issn'],
					);

					ImporterModel::insertProduct($data);

					$id_supplier = Supplier::getIdByName('Azymut');

					Importer::updateProductSupplier($product->id, $id_supplier, $book['cenaDet']);

					$id_feature = ImporterModel::getFeatureIdByName('Autor');
					foreach ($authors as $author)
					{
						$id_feature_val = ImporterModel::getFeatureValueIdByValue($author);

						if ($id_feature_val == false)
						{
							$feature_val = new FeatureValue();
							$feature_val->id_feature = $id_feature;

							foreach ($languages as $lang)
							{
								$feature_val->value[$lang['id_lang']] = $author;
							}

							$feature_val->save();

							$id_feature_val = $feature_val->id;
						}

						if (ImporterModel::featureProductExist($id_feature, $product->id, $id_feature_val) == false)
						{
							$product->addFeaturesToDB($id_feature, $id_feature_val);
						}
					}
				}
				catch (Exception $ex) 
				{
					$this->saveLog(json_encode($book));
					$this->lockCron();
					$this->saveLog($ex);
					$error = true;
				}
			}
		}
		
		if (!$error)
		{
			$this->confirmTransaction($this->transaction_Id);
		}
		
		$this->saveLog('[Azymut] End: getdb.');
	}
	
	public function getPriceData($mode)
	{
		$this->saveLog('[Azymut] Start: getPrice.');
		
		$file = __DIR__.'/price.xml';

		$xml = $this->getXmlData($mode, $file);
		
		if ($xml)
		{
			$attrs = array('indeks', 'cena', 'vat', 'detal');
			
			$last_index = (int)ImporterModel::getOptionValueByName('last_price_id');
			$last_product_index = $last_index;
			
			$data = $xml->book;
			
			$n = count($data);
			
			ImporterModel::updateMaxValueByName('last_price_id', $n);
			
			for($i = $last_index; $i <= $last_index+$this->xml_limit_data; $i++)
			{
				if ($data[$i] != null)
				{
					foreach ($attrs as $attr)
					{
						$ind2cat_attr[$attr] = (string)$data[$i][$attr];
					}
					
					if (($prod_id = ImporterModel::getProductIdByAzymutIndex($ind2cat_attr['indeks'])) != false)
					{
						$price_data = array(
							'azymut_price' => $ind2cat_attr['detal'],
							'azymut_wholesale' => $ind2cat_attr['cena'],
							'azymut_vat' => $ind2cat_attr['vat'],
						);

						$where = 'id='.$prod_id;

						Db::getInstance()->update(ImporterModel::TAB_PRODS, $price_data, $where);
					}
					
					$last_product_index = $i;
				}

				ImporterModel::updateOptionValueByName('last_price_id', $i);

				if ($i >= $n)
				{
					mail('support@tomsky.pl', '[Azymut] Price Cron - End', 'Job done.');
					ImporterModel::updateOptionValueByName('last_price_id', '0');
					unlink($file);
					break;
				}
			}
		
			$this->saveLog('[Azymut] Last index: '.($last_product_index));
		}
		
		$this->saveLog('[Azymut] End: getPrice.');
	}
	
	/**
	 * Get image from Azymut and add image to product like cover.
	 * @param string $mode
	 * @param bool
	 */
	public function getImgData($mode, $update = false)
	{
		$this->saveLog('[Azymut] Start: getImg.');
		
		$products = ImporterModel::getAllProducts();
		$products_count = count($products);
		
		$last_image_id = (int)ImporterModel::getOptionValueByName('last_image_id');
		$last_product_id = $last_image_id;
		
		ImporterModel::updateMaxValueByName('last_image_id', $products_count);
		
		$languages = Language::getLanguages(true);
		
		$i = 0;
		$limit = 0;

		foreach ($products as $product)
		{
			$i++;
			
			if (($last_image_id != false) && ($last_image_id < $product['id']))
			{
				$ps_product = new Product($product['id']);

				$have_img = false;
				foreach ($languages as $lang)
				{
					$images = $ps_product->getImages($lang['id_lang']);

					if (!empty($images))
					{
						$have_img = true;
					}
				}

				if (!$have_img || $update)
				{
					$link = '/img/tmp/importer_image_tmp_'.$product['id'].'.jpg';

					if (file_exists($_SERVER["DOCUMENT_ROOT"].$link))
					{
						unlink($_SERVER["DOCUMENT_ROOT"].$link);
					}

					$image_data = $this->getDataFromAzymut($mode, '&indeks='.$product['id_azymut']);
					
					if ($image_data != false)
					{
						$handle = fopen($_SERVER["DOCUMENT_ROOT"].$link, 'x');
						fwrite($handle, $image_data);

						$ps_product->deleteImages();

						$image = new Image();
						$image->id_product = $product['id'];
						$image->deleteCover($product['id']);
						$image->cover = true;
						$image->add();

						Importer::copyImg($product['id'], $image->id, $_SERVER["DOCUMENT_ROOT"].$link);

						unlink($_SERVER["DOCUMENT_ROOT"].$link);
					}
					
					// Azymut time limit per one loop.
					sleep(2);
				}

				$limit++;
				$last_product_id = $product['id'];
				ImporterModel::updateOptionValueByName('last_image_id', $product['id']);
			}

			if ($products_count <= $i)
			{
				ImporterModel::updateOptionValueByName('last_image_id', '0');
			}

			if ($limit >= $this->img_limit_pl)
			{
				break;
			}
		}
		
		$this->saveLog('[Azymut] Last Product id: '.$last_product_id);
		
		$this->saveLog('[Azymut] End: getImg.');
	}
	
	/**
	 * Get library categories.
	 * @param string $mode
	 */
	public function getCatsData($mode)
	{
		$this->saveLog('[Azymut] Start: getCats.');
		
		$xml_data = $this->getDataFromAzymut($mode);
		
		if ($xml_data)
		{	
			$xml = simplexml_load_string($xml_data, "SimpleXMLElement", LIBXML_NOCDATA);
			$data = $this->xmlToArray($xml);
			
			$id_azymut = 'KC';
			
			foreach ($data['cats']['cat'] as $category)
			{
				$id_parent = ImporterModel::getParentIdByIdazymut($id_azymut);
				
				if ($id_parent != false && !ImporterModel::categoryExist($id_azymut, $category['@id']))
				{
					$sub_cat = new Category();
								
					foreach (Language::getLanguages(true) as $lang)
					{
						$sub_cat->name[$lang['id_lang']] = $category['$'];
						$sub_cat->link_rewrite[$lang['id_lang']] = Tools::link_rewrite($category['$']);
					}

					$sub_cat->active = 1;
					$sub_cat->id_parent = $id_parent;
					$sub_cat->add();

					ImporterModel::insertCategory(
						$sub_cat->id,
						$id_azymut,
						$category['@id'],
						$id_parent
					);
				}
			}
		}
		
		$this->saveLog('[Azymut] End: getCats.');
	}
	
	/**
	 * Get all categories.
	 * @param string $mode
	 */
	public function getDzialyData($mode)
	{
		$this->saveLog('[Azymut] Start: getDzialy.');
		
		$xml_data = $this->getDataFromAzymut($mode);
		
		if ($xml_data)
		{
			$xml = simplexml_load_string($xml_data, "SimpleXMLElement", LIBXML_NOCDATA);
			$data = $this->xmlToArray($xml);
			
			foreach ($data['dzialy']['d'] as $dzial)
			{
				if (($dzial['@id'] == 'KC') ||
					($dzial['@id'] == 'ZBW'))
				{
					$id_parent = ImporterModel::categoryExist($dzial['@id'], '0');
					
					if (!$id_parent)
					{
						$cat = new Category();

						foreach (Language::getLanguages(true) as $lang)
						{
							$cat->name[$lang['id_lang']] = $dzial['nazwa'];
							$cat->link_rewrite[$lang['id_lang']] = Tools::link_rewrite($dzial['nazwa']);
						}

						$cat->active = 1;
						$cat->id_parent = Configuration::get('PS_HOME_CATEGORY');
						$cat->add();
						
						$id_parent = $cat->id;
						
						ImporterModel::insertCategory(
							$cat->id,
							$dzial['@id'],
							'0',
							0
						);
					}
					
					if ($dzial['@id'] == 'ZBW')
					{
						foreach ($dzial['kategorie']['k'] as $sub_category)
						{
							if (!ImporterModel::categoryExist($dzial['@id'], $sub_category['@id']))
							{
								$sub_cat = new Category();
								
								foreach (Language::getLanguages(true) as $lang)
								{
									$sub_cat->name[$lang['id_lang']] = $sub_category['$'];
									$sub_cat->link_rewrite[$lang['id_lang']] = Tools::link_rewrite($sub_category['$']);
								}
								
								$sub_cat->active = 1;
								$sub_cat->id_parent = $id_parent;
								$sub_cat->add();
								
								ImporterModel::insertCategory(
									$sub_cat->id,
									$dzial['@id'],
									$sub_category['@id'],
									$id_parent
								);
							}
						}
					}
				}
			}
		}
		
		$this->saveLog('[Azymut] End: getDzialy.');
	}
	
	/**
	 * Get product stock data.
	 * @param string $mode
	 */
	public function getAvailData($mode)
	{
		$this->saveLog('[Azymut] Start: getIAvail.');
		
		$file = __DIR__.'/avail.xml';

		$xml = $this->getXmlData($mode, $file);
		
		if ($xml)
		{
			$attrs = array('indeks', 'p');
			
			$last_index = (int)ImporterModel::getOptionValueByName('last_avail_id');
			$last_product_index = $last_index;
			
			$data = $xml->book;
			
			$n = count($data);
			
			ImporterModel::updateMaxValueByName('last_avail_id', $n);
			
			for($i = $last_index; $i <= $last_index+$this->xml_limit_data; $i++)
			{
				if ($data[$i] != null)
				{
					foreach ($attrs as $attr)
					{
						$ind2cat_attr[$attr] = (string)$data[$i][$attr];
					}
					
					if (($prod_id = ImporterModel::getProductIdByAzymutIndex($ind2cat_attr['indeks'])) != false)
					{
						$stock_id = StockAvailable::getStockAvailableIdByProductId($prod_id);

						if ($ind2cat_attr['p'] != '')
						{
							$stock = new StockAvailable($stock_id);
							$stock->id_product = $prod_id;

							$stock->quantity = (int)$ind2cat_attr['p'];
							$stock->save();
						}
					}
					
					$last_product_index = $i;
				}

				ImporterModel::updateOptionValueByName('last_avail_id', $i);

				if ($i >= $n)
				{
					ImporterModel::updateOptionValueByName('last_avail_id', '0');
					unlink($file);
					break;
				}
			}
		
			$this->saveLog('[Azymut] Last index: '.($last_product_index));
		}
		
		$this->saveLog('[Azymut] End: getIAvail.');
	}
	
	public function getItemData($mode, $index)
	{
		$data = $this->getDataFromAzymut($mode, '&ind='.$index);
	}
	
	/**
	 * Add categories to product.
	 * @param string $mode
	 */
	public function getInd2CatData($mode)
	{
		$this->saveLog('[Azymut] Start: getInd2Cat.');
		
		$file = __DIR__.'/ind2cat.xml';

		$xml = $this->getXmlData($mode, $file);
		
		if ($xml)
		{
			$attrs = array('id', 'ind');
			
			$last_index = (int)ImporterModel::getOptionValueByName('last_category_id');
			$last_product_index = $last_index;
			
			$data = $xml->cat;
			
			$n = count($data);
			
			ImporterModel::updateMaxValueByName('last_category_id', $n);
			
			for($i = $last_index; $i <= $last_index+$this->xml_c2e_limit_data; $i++)
			{
				$categories = [];
				
				if ($data[$i] != null)
				{
					foreach ($attrs as $attr)
					{
						$ind2cat_attr[$attr] = (string)$data[$i][$attr];
					}
					

					if ((($prod_id = ImporterModel::getProductIdByAzymutIndex($ind2cat_attr['ind'])) != false) && 
						(($cat_id = ImporterModel::getPrestaIdCatByIdAzymutCat($ind2cat_attr['id'])) != false))
					{
						$product = new Product($prod_id);

						$categories[] = $cat_id;

						try
						{
							$product->addToCategories($categories);
							$product->id_category_default = $cat_id;
							$product->save();
						} 
						catch (Exception $ex) 
						{
							//$this->saveLog($ex);
						}
					}
					
					$last_product_index = $i;
				}

				ImporterModel::updateOptionValueByName('last_category_id', $i);

				if ($i >= $n)
				{
					ImporterModel::updateOptionValueByName('last_category_id', '0');
					unlink($file);
					break;
				}
			}
		
			$this->saveLog('[Azymut] Last index: '.($last_product_index));
		}
		
		$this->saveLog('[Azymut] End: getInd2Cat.');
	}
	
	/**
	 * Get Data from Azymut by method type.
	 * @param string $mode Method type: confirm, getImg, getAvail, getPrice, getdb, getCats, getDzialy, getInd2Cat, getItem.
	 * @param string additional vars.
	 * @return string XML or binary for image.
	 */
	public function getDataFromAzymut($mode, $attr = '')
	{
		try
		{
			$url  = $this->azymut_url;
			$url .= $this->azymut_serv_url;
			$url .= '?mode='.$mode;
			$url .= '&id='.$this->azymut_id;
			$url .= '&p='.$this->azymut_passwd;
			$url .= $attr;

			$ch = curl_init($url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			$html = curl_exec($ch);

			curl_close($ch);

			$pos = [];
			$pos[0] = strpos($html, 'ERROR: ');
			$pos[1] = strpos($html, 'YOU ASK TO OFTEN');
			
			if ($pos[0] != false || $pos[1] != false)
			{
				if ($pos[0] != false)
				{
					Importer::lockCron();
				}
				
				$this->saveLog($html);
				$html = false;
			}
		}
		catch (Exception $ex)
		{
			$this->saveLog($ex);
			$html = false;
		}
		
		return $html;
	}
	
	/**
	 * Make product short decription.
	 * @param array $data
	 * @return string
	 */
	private function makeShortDescription($data)
	{
		$description = '';
				
		if (!empty($data['isbn']))
		{
			$description .= 'ISBN: <b>'.$data['isbn'].'</b>';

			if (!empty($data['tytuloryg']))
			{
				$description .= '<br />Tytuł oryginalu: <b>'.$data['tytuloryg'].'</b>';
			}
			if (!empty($data['jezykoryg']))
			{
				$description .= '<br />Język oryginalu: <b>'.$data['jezykoryg'].'</b>';
			}
			if (!empty($data['tlumacze']))
			{
				$description .= '<br />Tłumaczenie: <b>'.$data['tlumacze'].'</b>';
			}
			if (!empty($data['redakcja']))
			{
				$description .= '<br />Redakcja: <b>'.$data['redakcja'].'</b>';
			}
			if (!empty($data['oprawa']))
			{
				$description .= '<br />Oprawa: <b>'.$data['oprawa'].'</b>';
			}
			if (!empty($data['format']))
			{
				$description .= '<br />Format: <b>'.$data['format'].'</b>';
			}
			if (!empty($data['objetosc']))
			{
				$description .= '<br />Liczba stron: <b>'.$data['objetosc'].'</b>';
			}
			if (!empty($data['wydanie']))
			{
				$description .= '<br />Wydanie: <b>'.$data['wydanie'].'</b>';
			}
			if (!empty($data['rokwyd']))
			{
				$description .= '<br />Rok wydania: <b>'.$data['rokwyd'].'</b>';
			}
			if (!empty($data['kodwydawcy']))
			{
				$description .= '<br />Kod wydawcy: <b>'.$data['kodwydawcy'].'</b>';
			}
			if (!empty($data['ciezar']))
			{
				$description .= '<br />Waga: <b>'.$data['ciezar'].'</b>'; 
			}
			if (!empty($data['tytultomu']))
			{
				$description .= '<br />Tytuł tomu: <b>'.$data['tytultomu'].'</b>';
			}
			if (!empty($data['nrkolejnyt']))
			{
				$description .= '<br />Numer tomu: <b>'.$data['nrkolejnyt'].'</b>';
			}
			if (!empty($data['liczbatomo']))
			{
				$description .= '<br />Liczba tomów: <b>'.$data['liczbatomo'].'</b>';
			}
			if (!empty($data['pkwiu']))
			{
				$description .= '<br />PKWIU: <b>'.$data['pkwiu'].'</b>'; 
			}
			if (!empty($data['issn']))
			{
				$description .= '<br />Kod paskowy EAN: <b>'.$data['issn'].'</b>'; 
			}
		}
		
		return Importer::cleanString($description);
	}
	
}
