<?php

/**
 * Importer main class.
 *
 * @author Paweł Kujaczyński for Tomsky Sp. z o.o.
 */
class Importer
{
	/**
	 * Save log to file.
	 * @var bool
	 */
	public $saveLog = true;
	
	/**
	 * Echo log message on screen.
	 * @var bool
	 */
	public $debug = true;
	
	
	
	
	public function __construct($debug)
	{
		$this->debug = $debug;
	}
	
	/**
	 * Get xml data from file or get data from Azymut and write to file.
	 * @param string $mode
	 * @param string $file
	 * @return string
	 */
	public function getXmlData($mode, $file)
	{
		$xml = false;
		
		if (!file_exists($file))
		{
			$xml_data = $this->getDataFromAzymut($mode);
			
			$handle = fopen($file, 'a+');
			fwrite($handle, $xml_data);
			
			$xml = simplexml_load_string($xml_data);
		}
		else
		{
			$handle = fopen($file, 'a+');
			$xml_data = fread($handle, filesize($file));
			
			$xml = simplexml_load_string($xml_data);
		}
		fclose($handle);
		
		return $xml;
	}
	
	/**
	 * Set best product price from providers.
	 * @param int $prod_id
	 */
	public function setBestProductPrice($prod_id)
	{
		$product_data = ImporterModel::getProductById($prod_id);
		
		$price = $this->getArrayBestPrice($product_data);
		
		$product = new Product($prod_id);
		$product->price = $price['price'];
		$product->wholesale_price = $price['wholesale_price'];
		$product->id_tax_rules_group = ImporterModel::getTaxIdByRate($price['vat']);
		$product->save();
	}
	
	/**
	 * Set best product price from providers for all products .
	 */
	public function setAllBestProductPrice()
	{
		$products_data = ImporterModel::getAllProducts();
		
		$product_count = count($products_data);
		
		ImporterModel::updateMaxValueByName('last_best_price_id', $product_count);
		
		$last_index = ImporterModel::getOptionValueByName('last_best_price_id');
		
		$count = 0;
		foreach ($products_data as $product)
		{
			$count++;
			if ($count >= $last_index)
			{
				$price = $this->getArrayBestPrice($product);

				$product = new Product($product['id']);
				$product->price = $price['price'];
				$product->wholesale_price = $price['wholesale_price'];
				$product->id_tax_rules_group = ImporterModel::getTaxIdByRate($price['vat']);
				$product->save();
				
				ImporterModel::updateOptionValueByName('last_best_price_id', $count);
			}
			
			if ($count >= $product_count)
			{
				ImporterModel::updateOptionValueByName('last_best_price_id', '0');
				
				break;
			}
			
		}
		
	}
	
	/**
	 * Get best price from providers.
	 * @param array $product_data
	 * @return array
	 */
	public function getArrayBestPrice($product_data)
	{
		$price['price'] = $product_data['azymut_price'];
		$price['wholesale_price'] = $product_data['azymut_wholesale'];
		$price['vat'] = $product_data['azymut_vat'];
		
		$supplier_name = 'Azymut';
		
		if ($product_data['azymut_price'] <= $product_data['ss_price'])
		{
			if (($product_data['azymut_price'] > $product_data['ateneum_price']) &&
				($product_data['ateneum_price'] > 0.0))
			{
				$price['price'] = $product_data['ateneum_price'];
				$price['wholesale_price'] = $product_data['ateneum_wholeprice'];
				$price['vat'] = $product_data['ateneum_vat'];
				
				$supplier_name = 'Ateneum';
			}
			else
			{
				if ($product_data['azymut_price'] > 0.0)
				{
					$price['price'] = $product_data['azymut_price'];
					$price['wholesale_price'] = $product_data['azymut_wholesale'];
					$price['vat'] = $product_data['azymut_vat'];
				
					$supplier_name = 'Azymut';
				}
			}
		}
		else
		{
			if (($product_data['ss_price'] > $product_data['ateneum_price']) &&
				($product_data['ateneum_price'] > 0.0))
			{
				$price['price'] = $product_data['ateneum_price'];
				$price['wholesale_price'] = $product_data['ateneum_wholeprice'];
				$price['vat'] = $product_data['ateneum_vat'];
				
				$supplier_name = 'Ateneum';
			}
			else
			{
				if ($product_data['ss_price'] > 0.0)
				{
					$price['price'] = $product_data['ss_price'];
					$price['wholesale_price'] = $product_data['ss_wholesale'];
					$price['vat'] = $product_data['ss_vat'];
				
					$supplier_name = 'SuperSiodemka';
				}
				
			}
		}
		$id_supplier = Supplier::getIdByName($supplier_name);
		
		Importer::updateProductSupplier($product_data['id'], $id_supplier, $price['price']);
		
		return $price;
	}
	
	/**
	 * Remove product supplier and add actual supplier.
	 * @param int $id_product
	 * @param int $id_supplier
	 * @param float $price
	 */
	public static function updateProductSupplier($id_product, $id_supplier, $price)
	{
		$supplier_products = ProductSupplier::getSupplierCollection($id_product);
		
		if (empty($supplier_products[0]))
		{
			$product_supplier = new ProductSupplier();
	-		$product_supplier->id_currency = 1;
	-		$product_supplier->id_product_attribute = 0;
	-		$product_supplier->id_product = $id_product;
	-		$product_supplier->id_supplier = $id_supplier;
	-		$product_supplier->product_supplier_price_te = $price;
	-		$product_supplier->save();
		}
		else
		{
			$supplier_products[0]->id_supplier = $id_supplier;
			$supplier_products[0]->product_supplier_price_te = $price;
			$supplier_products[0]->update();
		}	
	}
	
	/**
	 * Converting an xml to array.
	 * @param string $xml_string
	 * @return array
	 */
	public function xmlToArray($xml, $options = array()) 
	{
		$defaults = array(
			'namespaceSeparator' => ':',//you may want this to be something other than a colon
			'attributePrefix' => '@',   //to distinguish between attributes and nodes with the same name
			'alwaysArray' => array(),   //array of xml tag names which should always become arrays
			'autoArray' => true,        //only create arrays for tags which appear more than once
			'textContent' => '$',       //key used for the text content of elements
			'autoText' => true,         //skip textContent key if node has no attributes or child nodes
			'keySearch' => false,       //optional search and replace on tag and attribute names
			'keyReplace' => false       //replace values for above search values (as passed to str_replace())
		);
		$options = array_merge($defaults, $options);
		$namespaces = $xml->getDocNamespaces();
		$namespaces[''] = null; //add base (empty) namespace

		//get attributes from all namespaces
		$attributesArray = array();
		foreach ($namespaces as $prefix => $namespace)
		{
			foreach ($xml->attributes($namespace) as $attributeName => $attribute) 
			{
				//replace characters in attribute name
				if ($options['keySearch']) $attributeName =
						str_replace($options['keySearch'], $options['keyReplace'], $attributeName);
				$attributeKey = $options['attributePrefix']
						. ($prefix ? $prefix . $options['namespaceSeparator'] : '')
						. $attributeName;
				$attributesArray[$attributeKey] = (string)$attribute;
			}
		}

		//get child nodes from all namespaces
		$tagsArray = array();
		foreach ($namespaces as $prefix => $namespace) 
		{
			foreach ($xml->children($namespace) as $childXml) 
			{
				
				//recurse into child nodes
				$childArray = $this->xmlToArray($childXml, $options);
				list($childTagName, $childProperties) = each($childArray);

				//replace characters in tag name
				if ($options['keySearch']) $childTagName =
						str_replace($options['keySearch'], $options['keyReplace'], $childTagName);
				//add namespace prefix, if any
				if ($prefix) $childTagName = $prefix . $options['namespaceSeparator'] . $childTagName;

				if (!isset($tagsArray[$childTagName])) 
				{
					//only entry with this key
					//test if tags of this type should always be arrays, no matter the element count
					$tagsArray[$childTagName] =
							in_array($childTagName, $options['alwaysArray']) || !$options['autoArray']
							? array($childProperties) : $childProperties;
				} 
				elseif (is_array($tagsArray[$childTagName]) && array_keys($tagsArray[$childTagName])
					=== range(0, count($tagsArray[$childTagName]) - 1)) 
				{
					//key already exists and is integer indexed array
					$tagsArray[$childTagName][] = $childProperties;
				} 
				else 
				{
					//key exists so convert to integer indexed array with previous value in position 0
					$tagsArray[$childTagName] = array($tagsArray[$childTagName], $childProperties);
				}
			}
		}

		//get text content of node
		$textContentArray = array();
		$plainText = trim((string)$xml);
		if ($plainText !== '') $textContentArray[$options['textContent']] = $plainText;

		//stick it all together
		$propertiesArray = !$options['autoText'] || $attributesArray || $tagsArray || ($plainText === '')
				? array_merge($attributesArray, $tagsArray, $textContentArray) : $plainText;

		//return node as array
		return array(
			$xml->getName() => $propertiesArray
		);
	}
	
	/**
	 * Get CSV data from url.
	 * @param string $url
	 * @param string $username
	 * @param string $password
	 * @return array [0]ident_ate,[2]ISBN,
	 */
	public function getDataFromUrlFile($url, $username, $password)
	{
		$csv_data = '';
		
		$context = stream_context_create(array(
			'http' => array(
				'header'  => "Authorization: Basic " . base64_encode("$username:$password")
			)
		));
		
		try
		{
			$csv_data = file_get_contents($url, false, $context);
		}
		catch (Exception $ex)
		{
			$this->saveLog($ex);
		}
		
		return $csv_data;
	}
	
	/**
	 * Get data from file on SFTP.
	 * @param string $ftp_server
	 * @param string $ftp_login
	 * @param string $ftp_passwd
	 * @param string $ftp_port
	 * @param string $ftp_file_dir
	 * @param string $file
	 * @return string
	 */
	public function getDataFromFtp($ftp_server, $ftp_login, $ftp_passwd, $ftp_port = '21', $ftp_file_dir, $file)
	{
		// set up basic connection
		$connection = ssh2_connect($ftp_server, $ftp_port);
		
		// login with username and password
		ssh2_auth_password($connection, $ftp_login, $ftp_passwd);
		
		$sftp = ssh2_sftp($connection);

		// check connection
		if ((!$connection) || (!$sftp))
		{
			$this->saveLog('[S7] FTP connection has failed!');
			$this->saveLog("[S7] Attempted to connect to $ftp_server for user $ftp_login.");
			
			return false; 
		}
		else 
		{
			$this->saveLog("[S7] Connected to $ftp_server, for user $ftp_login.");
		}
		
		$stream = fopen('ssh2.sftp://'.$sftp.$ftp_file_dir, 'r');
		
		$data_string = '';
		while (($buffer = fgets($stream)) !== false)
		{
			$data_string .= $buffer;
		}
		fclose($stream);
		
		$handle = fopen($file, 'a+');
		fwrite($handle, $data_string);
		fclose($handle);
		
		return true;
	}
	
	/**
     * copyImg copy an image located in $url and save it in a path
     * according to $entity->$id_entity .
     * $id_image is used if we need to add a watermark
     *
     * @param int $id_entity id of product or category (set in entity)
     * @param int $id_image (default null) id of the image if watermark enabled.
     * @param string $url path or url to use
     * @param string $entity 'products' or 'categories'
     * @param bool $regenerate
     * @return bool
     */
    public static function copyImg($id_entity, $id_image = null, $url, $entity = 'products', $regenerate = true)
    {
        $tmpfile = tempnam(_PS_TMP_IMG_DIR_, 'ps_import');
        $watermark_types = explode(',', Configuration::get('WATERMARK_TYPES'));

        switch ($entity) {
            default:
            case 'products':
                $image_obj = new Image($id_image);
                $path = $image_obj->getPathForCreation();
            break;
            case 'categories':
                $path = _PS_CAT_IMG_DIR_.(int)$id_entity;
            break;
            case 'manufacturers':
                $path = _PS_MANU_IMG_DIR_.(int)$id_entity;
            break;
            case 'suppliers':
                $path = _PS_SUPP_IMG_DIR_.(int)$id_entity;
            break;
        }

        $url = urldecode(trim($url));
        $parced_url = parse_url($url);

        if (isset($parced_url['path'])) {
            $uri = ltrim($parced_url['path'], '/');
            $parts = explode('/', $uri);
            foreach ($parts as &$part) {
                $part = rawurlencode($part);
            }
            unset($part);
            $parced_url['path'] = '/'.implode('/', $parts);
        }

        if (isset($parced_url['query'])) {
            $query_parts = array();
            parse_str($parced_url['query'], $query_parts);
            $parced_url['query'] = http_build_query($query_parts);
        }

        if (!function_exists('http_build_url')) {
            require_once(_PS_TOOL_DIR_.'http_build_url/http_build_url.php');
        }

        $url = http_build_url('', $parced_url);

        $orig_tmpfile = $tmpfile;

        if (Tools::copy($url, $tmpfile)) {
            // Evaluate the memory required to resize the image: if it's too much, you can't resize it.
            if (!ImageManager::checkImageMemoryLimit($tmpfile)) {
                @unlink($tmpfile);
                return false;
            }

            $tgt_width = $tgt_height = 0;
            $src_width = $src_height = 0;
            $error = 0;
            ImageManager::resize($tmpfile, $path.'.jpg', null, null, 'jpg', false, $error, $tgt_width, $tgt_height, 5,
                                 $src_width, $src_height);
            $images_types = ImageType::getImagesTypes($entity, true);

            if ($regenerate) {
                $previous_path = null;
                $path_infos = array();
                $path_infos[] = array($tgt_width, $tgt_height, $path.'.jpg');
                foreach ($images_types as $image_type) {
                    $tmpfile = self::get_best_path($image_type['width'], $image_type['height'], $path_infos);

                    if (ImageManager::resize($tmpfile, $path.'-'.stripslashes($image_type['name']).'.jpg', $image_type['width'],
                                         $image_type['height'], 'jpg', false, $error, $tgt_width, $tgt_height, 5,
                                         $src_width, $src_height)) {
                        // the last image should not be added in the candidate list if it's bigger than the original image
                        if ($tgt_width <= $src_width && $tgt_height <= $src_height) {
                            $path_infos[] = array($tgt_width, $tgt_height, $path.'-'.stripslashes($image_type['name']).'.jpg');
                        }
                        if ($entity == 'products') {
                            if (is_file(_PS_TMP_IMG_DIR_.'product_mini_'.(int)$id_entity.'.jpg')) {
                                unlink(_PS_TMP_IMG_DIR_.'product_mini_'.(int)$id_entity.'.jpg');
                            }
                            if (is_file(_PS_TMP_IMG_DIR_.'product_mini_'.(int)$id_entity.'_'.(int)Context::getContext()->shop->id.'.jpg')) {
                                unlink(_PS_TMP_IMG_DIR_.'product_mini_'.(int)$id_entity.'_'.(int)Context::getContext()->shop->id.'.jpg');
                            }
                        }
                    }
                    if (in_array($image_type['id_image_type'], $watermark_types)) {
                        Hook::exec('actionWatermark', array('id_image' => $id_image, 'id_product' => $id_entity));
                    }
                }
            }
        }
		else
		{
            @unlink($orig_tmpfile);
            return false;
        }
        unlink($orig_tmpfile);
        return true;
    }

    private static function get_best_path($tgt_width, $tgt_height, $path_infos)
    {
        $path_infos = array_reverse($path_infos);
        $path = '';
		
        foreach ($path_infos as $path_info)
		{
            list($width, $height, $path) = $path_info;
			
            if ($width >= $tgt_width && $height >= $tgt_height)
			{
                return $path;
            }
        }
		
        return $path;
    }
	
	/**
	 * Remove prestashop special characters from string.
	 * @param string $string
	 * @return string
	 */
	public static function cleanString($string)
	{
		$special_chars = ['#', '$', '^', '^', ';', '=', '{', '}'];
		
		$text = htmlspecialchars($string);
		
		foreach ($special_chars as $char)
		{
			$text = str_replace($char, ' ' , $text);
		}
		
		$text2 = preg_replace('/.*script\:/ims', '/.*script \:/ims', $text);
		
		return $text2;
	}
	
	/**
	 * Save message to log file.
	 * @param type $message
	 * @return bool false or number of save byte.
	 */
	public function saveLog($message)
	{
		if ($this->debug)
		{
			echo $message.'<br /><br />';
		}
		
		$result = false;
		if ($this->saveLog)
		{
			$handle = fopen(__DIR__.'/tomskyimporterlog.txt', 'a');
			$result = fwrite($handle, $message.PHP_EOL);
			fclose($handle);
		}
		
		return $result;
	}
	
	/**
	 * Lock cron job if is error.
	 */
	public static function lockCron()
	{
		$handle = fopen(__DIR__.'/cron_error', 'a');
		
		fwrite($handle, 'ERROR - LOCK CRON JOB');
		fclose($handle);
	}
	
	/**
	 * Print formated data.
	 * @param mixed $data
	 */
	public function prePrint($data)
	{
		echo '<pre>';
		print_r($data);
		echo '</pre>';
	}
}
