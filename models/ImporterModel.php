<?php

/**
 * Importer databes model.
 *
 * @author Paweł Kujaczyński for Tomsky Sp. z o.o.
 */
class ImporterModel
{
	const TAB_CATS = 'ti_categories';
	
	const TAB_PRODS = 'ti_products';
	
	/**
	 * Check if there is category in importer
	 * @param string $id_azymut
	 * @param string $id_azymut_cat
	 * @return bool | false | number
	 */
	public static function categoryExist($id_azymut, $id_azymut_cat)
	{
		$sql  = 'SELECT id FROM `'._DB_PREFIX_.ImporterModel::TAB_CATS.'`';
		$sql .= ' WHERE id_azymut="'.$id_azymut.'" AND id_azymut_cat="'.$id_azymut_cat.'"';
			
		$result = Db::getInstance()->getValue($sql);
		
		return $result;
	}
	
	/**
	 * Insert category to importer.
	 * @param int $id
	 * @param string $id_azymut
	 * @param string $id_azymut_cat
	 * @param int $id_parent
	 * @return bool
	 */
	public static function insertCategory($id, $id_azymut, $id_azymut_cat, $id_parent)
	{
		$data = array(
			'id' => $id,
			'id_azymut' => $id_azymut,
			'id_azymut_cat' => $id_azymut_cat,
			'id_parent' => $id_parent
		);
		$return = Db::getInstance()->insert(ImporterModel::TAB_CATS, $data);
		
		return $return;
	}
	
	/**
	 * Get Parent Id by Id_azymut.
	 * @param string $id_azymut
	 * @return int | bool
	 */
	public static function getParentIdByIdazymut($id_azymut)
	{
		$sql  = 'SELECT id FROM `'._DB_PREFIX_.ImporterModel::TAB_CATS.'`';
		$sql .= ' WHERE id_azymut="'.$id_azymut.'" AND id_parent=0';
		
		$id_parent = Db::getInstance()->getValue($sql);
		
		return $id_parent;
	}
	
	/**
	 * Get Presta Category Id by Azymut category id.
	 * @param int $id_azymut_cat
	 * @return int
	 */
	public static function getPrestaIdCatByIdAzymutCat($id_azymut_cat)
	{
		$sql  = 'SELECT id FROM `'._DB_PREFIX_.ImporterModel::TAB_CATS.'`';
		$sql .= ' WHERE id_azymut_cat="'.$id_azymut_cat.'"';
		
		$id_presta_cat = Db::getInstance()->getValue($sql);
		
		return $id_presta_cat;
	}
	
	/**
	 * Insert product if not exist.
	 * @param array $data
	 * @return bool | int
	 */
	public static function insertProduct($data)
	{
		$return = true;
		
		$sql = 'SELECT id FROM `'._DB_PREFIX_.ImporterModel::TAB_PRODS.'` WHERE id='.$data['id'];
		$result = Db::getInstance()->getValue($sql);
		
		if ($result == false)
		{
			$return = Db::getInstance()->insert(ImporterModel::TAB_PRODS, $data);
		}
		
		return $return;
	}
	
	/**
	 * Get importer product data.
	 * @param int $prod_id
	 * @return array
	 */
	public static function getProductById($prod_id)
	{
		$sql = 'SELECT * FROM `'._DB_PREFIX_.ImporterModel::TAB_PRODS.'`';
		$sql .= ' WHERE id="'.$prod_id.'"';
		
		$product = Db::getInstance()->getRow($sql);
		
		return $product;
	}
	
	public static function getProductIdByAzymutIndex($azymut_index)
	{
		$sql  = 'SELECT id FROM `'._DB_PREFIX_.ImporterModel::TAB_PRODS.'`';
		$sql .= ' WHERE id_azymut="'.$azymut_index.'"';
		
		$id_presta_prod = Db::getInstance()->getValue($sql);
		
		return $id_presta_prod;
	}
	
	public static function getProductIdByIsbn($isbn)
	{
		$sql = 'SELECT id FROM `'._DB_PREFIX_.ImporterModel::TAB_PRODS.'` WHERE isbn="'.$isbn.'"';
			
		return Db::getInstance()->getValue($sql);
	}
	
	public static function getAllProducts()
	{
		$sql = 'SELECT * FROM `'._DB_PREFIX_.ImporterModel::TAB_PRODS.'`';
			
		return Db::getInstance()->executeS($sql);
	}
	
	public static function getTaxIdByRate($rate)
	{
		$sql = 'SELECT id_tax FROM `'._DB_PREFIX_.'tax` WHERE rate LIKE "'.floatval($rate).'%"';
		
		return Db::getInstance()->getValue($sql);
	}
	
	public static function getAttrGroupIdByName($name)
	{
		$sql = 'SELECT id_attribute_group FROM `'._DB_PREFIX_.'attribute_group_lang` WHERE name="'.$name.'"';
		
		return Db::getInstance()->getValue($sql);
	}
	
	public static function getAttrIdByName($name)
	{
		$sql = 'SELECT id_attribute FROM `'._DB_PREFIX_.'attribute_lang` WHERE name="'.$name.'"';
		
		return Db::getInstance()->getValue($sql);
	}
	
	public static function getFeatureIdByName($name)
	{
		$sql = 'SELECT id_feature FROM `'._DB_PREFIX_.'feature_lang` WHERE name="'.$name.'"';
		
		return Db::getInstance()->getValue($sql);
	}
	
	public static function getFeatureValueIdByValue($value)
	{
		$sql = 'SELECT id_feature_value FROM `'._DB_PREFIX_.'feature_value_lang` WHERE value="'.$value.'"';
		
		return Db::getInstance()->getValue($sql);
	}
	
	public static function removeAllProductSupplier($id_product)
	{
		$where = 'id_product='.$id_product;
			
		return Db::getInstance()->delete(_DB_PREFIX_.'product_supplier', $where);
	}
	
	public static function featureProductExist($id_feature, $id_product, $id_feature_value)
	{
		$sql  = 'SELECT id_feature_value FROM `'._DB_PREFIX_.'feature_product`';
		$sql .= ' WHERE id_feature="'.$id_feature.'" AND id_product='.$id_product.' AND id_feature_value='.$id_feature_value;
		
		return Db::getInstance()->getValue($sql);
	}
	
	/**
	 * Get option value by option name.
	 * @param string $name
	 * @return bool | string
	 */
	public static function getOptionValueByName($name)
	{
		$sql  = 'SELECT option_value FROM `'._DB_PREFIX_.'ti_options`';
		$sql .= ' WHERE option_name ="'.$name.'"';
		
		return Db::getInstance()->getValue($sql);
	}
	
	/**
	 * Update option value by option name.
	 * @param string $name
	 * @param string $value
	 * @return bool
	 */
	public static function updateOptionValueByName($name, $value)
	{
		$data = ['option_value' => $value];
		$where = 'option_name = "'.$name.'"';
		
		return Db::getInstance()->update('ti_options', $data, $where);
	}
	
	/**
	 * Update max value by option name.
	 * @param string $name
	 * @param string $value
	 * @return bool
	 */
	public static function updateMaxValueByName($name, $value)
	{
		$data = ['max_value' => $value];
		$where = 'option_name = "'.$name.'"';
		
		return Db::getInstance()->update('ti_options', $data, $where);
	}
}
